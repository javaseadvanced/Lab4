# Exercise4
Потоки

# Содержание файла "MemoryDaemon.java"

public class MemoryDaemon implements Runnable {

	public MemoryDaemon() {

	}

	@Override
	public void run() {

	}

}

# Скопируйте указанный ниже код
# Вставьте с ПОЛНОЙ заменой в файл tasks.json
# Расположение файла tasks.json: Explorer => первая папка ".theia" => tasks.json

{
    "tasks": [
        {
            "type": "che",
            "label": "OnlineMedia build and run",
            "command": "javac -sourcepath /projects/Exercise8/Threads/src -d /projects/Exercise8/Threads/bin /projects/Exercise8/Threads/src/*.java && java -classpath /projects/Exercise8/Threads/bin OnlineMedia",
            "target": {
                "workingDir": "${CHE_PROJECTS_ROOT}/Exercise8/Threads/src",
                "component": "maven"
            }
        }
    ]
}
