
public class Track implements Playable,
		Comparable<Track> {
	private String title;
	private int length;
	public Track() {
		
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public void play() {
		System.out.println("Playing track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
		}
	public int compareTo(Track t2) {
		return (this.getTitle()).compareTo(t2.getTitle());
	} 
//	public int compareTo(Track t2) {
//		return Float.compare(this.getLength(), t2.getLength());
//	} 
}
